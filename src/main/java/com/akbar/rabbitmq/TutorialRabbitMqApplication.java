package com.akbar.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TutorialRabbitMqApplication {

	public static void main(String[] args) {
		SpringApplication.run(TutorialRabbitMqApplication.class, args);
	}

}
