package com.akbar.rabbitmq.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductService {
	
	@Autowired
	RabbitMQSender rabbitMQSender;
	
	@Autowired
	ProductRepository productRepository;

	public String testRabbit(ProductAddModel product) {
		rabbitMQSender.send(product);
		return "Message sent to the RabbitMQ Successfully";
	}
	
	public void testRabbitListener(ProductAddModel addProduct) {
		System.out.println("Masuk");
		Product product = new Product(addProduct.getName(), addProduct.getPrice());
		productRepository.save(product);
	}
	
}
