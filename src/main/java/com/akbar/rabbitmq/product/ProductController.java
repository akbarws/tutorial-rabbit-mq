package com.akbar.rabbitmq.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {
	
	@Autowired
	ProductService service;

	@PostMapping(value = "halo", consumes = "application/json")
	public String testRabbit(@RequestBody ProductAddModel product) {
		return service.testRabbit(product);
	}
	
}
