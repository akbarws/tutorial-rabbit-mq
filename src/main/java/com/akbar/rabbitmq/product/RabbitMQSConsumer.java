package com.akbar.rabbitmq.product;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RabbitMQSConsumer {
	
	@Autowired
	ProductService productService;

	@RabbitListener(queues = RabbitMQConfig.QUEUE)
	public void receiveMessage(ProductAddModel product) {
		System.out.println("Consume");
		productService.testRabbitListener(product);
	}
	
}
